/***
FN:F79506
PID:3
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;


int a=0;//position
double b=0.00;//number

 void exe_add(vector<double>&A)
 {
 	cin>>a>>b;
 	A.insert(A.begin()+(a-1),b);
 	
 }
 void remove(vector<double>&A)
 {
 	cin>>a;
 	A.erase(A.begin()+(a-1));
 }
 
 void reverse(vector<double>&A,const int &count)
 {
 	
	 for(int i=A.size();i>0;i--)
 	{
 		A.push_back(A[i-1]);
 	} 
 		A.erase(A.begin(),A.begin()+count);
		 
 }
 void shift_left(vector<double>&A,const int &count)
 {	
 		A.insert(A.begin(),A[count-(count-1)]);
 		A.erase(A.begin()+2);
 		A.push_back(A[1]);
 		A.erase(A.begin()+1);
 }
 
 void print(vector<double>&A)
 {
 	for(int i=0;i<A.size();i++)
 	{
 		
 		if(i==A.size()-1)
 		cout<<A[i]<<" "<<endl;
 		else
 		cout<<A[i]<<" ";
	 }
 }


 void command(const string &cmd,vector<double>&A, const int &count)
 {
 	if(cmd=="add")
 	 exe_add(A);
 	else if(cmd=="remove")
 	    remove(A);
 	else if(cmd=="reverse")
 	    reverse(A,count);
 	else if(cmd=="shift_left")
	    shift_left(A,count);    
 	else if(cmd=="print")
		 print(A);
    else
      cout<<"Unknown command"<<endl;
    
 }


int main()

{
	//code here
	vector<double>A;
	int n=0;
	double num=0.00;
	string cmd;
	cin>>n;
	for(int i=0;i<n;i++)
	{
		cin>>num;
		A.push_back(num);
		
	}
	int count=A.size();
	while(cin>>cmd)
	{
		command(cmd,A,count);
		if(cmd=="exit")
		
		break;
		
	}
	

	return 0;
}
