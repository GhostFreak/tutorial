/***
FN:F79506
PID:2
GID:1
*/

#include <iostream>
#include <math.h>

using namespace std;

//functions here
double prosta_lihva(double deposit, double rate,double years)
{
	 return deposit*(1+((rate/100)*years));
}
double slojna_lihva(double deposit,double rate,double years)
{
	return deposit*(pow((1+(rate/100)),years));
}

int main()
{
	//code here
	double deposit,rate,years;
	cout<<"Enter deposit amount, annual interest rate and years: "<<endl;
	cin>>deposit>>rate>>years;
	double result_1=prosta_lihva(deposit,rate,years);
    double result_2=slojna_lihva(deposit,rate,years);

	cout<<"In simple interest: "<<result_1<<endl;
	cout<<"In compound interest: "<<result_2<<endl;
	
	return 0;
}
