/***
FN:F79506
PID:3
GID:3
*/

#include <iostream>
#include <string>

using namespace std;

//functions here
void do_the_thing(double grade ,string fnomer, string result)
{
		string result1 ="PASS";
		string result2 ="FAIL";
		string result3 ="INVALID";
		if(grade>=3.00 && grade<=6.00)
		{
			 cout<<fnomer<<"\t"<<result1<<endl;
			
		}
		else if(grade<3.00 && grade>=2.00)
		{
			cout<<fnomer<<"\t"<<result2<<endl;
		}
		else
		{
			cout<<fnomer<<"\t"<<result3<<endl;
		}
		
}

int main()
{
	string fnomer ;
	double grade;
	string result="";
	
    while(cin>>fnomer>>grade)
	{
	
		 do_the_thing(grade,fnomer, result);
		
    }
	
	//code here
	return 0;
}
