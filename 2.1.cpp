/***
FN:F00000
PID:2
GID:1
*/

#include <iostream>
#include <limits>

using namespace std;

double do_the_thing(int random_number)
{
   int check=numeric_limits<int>::max();
   int num =random_number;
   double digits = 0;
    while (random_number) 
	{
        random_number /= 10;
        digits++;
    }
   
  for (int i=0;i<digits;i++)
{
	
    int numbers = num % 10;   
    num = (num - numbers)/10;
    
    if (numbers<check )
	{
		 check=numbers;
	}
     
    
}
    
	 return check;
	
}
int main()
{	
    int random_number;
	cin >> random_number;
	double result = do_the_thing(random_number);  
    cout << result;
	return 0 ;
}



