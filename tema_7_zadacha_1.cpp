/***
FN:F79506
PID:1
GID:1
*/

#include <iostream>

using namespace std;

//functions here
double add(double a,double b)
{
  return a+b;
}
double subtraction(double a,double b)
{
    return a-b;
}
double multiply(double a,double b)
{
    return a*b;
}
double divide(double a,double b)
{
    return a/b;
}
double do_the_math(double a,double b,char op)
{
    if (op =='+')
        return add(a,b);
    if (op =='-')
        return subtraction(a,b);
    if (op =='x')
        return multiply(a,b);
    if (op =='/')
        return divide(a,b);
    return -1;
}
int main()
{
	//code here
double lo,ro;
char op;
cout<<"Enter two numbers and a operation (+,-,x,/)"<<endl;
cin>>lo>>ro>>op;
double result =do_the_math(lo,ro,op);
cout<<"Result: "<<lo<<op<<ro<<"="<< result<<endl;

	return 0;
}
