/***
FN:F79506
PID:3
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;


int a=0;//position
double b=0.00;//number
double c=0.00;
double d=0.00;

 void exe_add(vector<double>&A)
 {
 	cin>>a>>b;
 	A.insert(A.begin()+(a-1),b);
 	
 }
 void remove(vector<double>&A)
 {
 	cin>>a;
 	A.erase(A.begin()+(a-1));
 }
 
 void reverse(vector<double>&A,const int &count)
 {
 	
	 for(int i=A.size();i>0;i--)
 	{
 		A.push_back(A[i-1]);
 	} 
 		A.erase(A.begin(),A.begin()+count);
		 
 }
 void shift_left(vector<double>&A)
 {	
 	
 		for(int i=0;i<A.size()-1;i++)
 		{
 			swap(A[i],A[i+1]);
		 }
 }
 void shift_right(vector<double>&A)
 {	
 	
 		for(int i=A.size()-1;i>0;i--)
 		{
 			swap(A[i],A[i-1]);
		 }
 }
 void merge(vector<double>&A)
 {	
 	vector<double>C;
 	vector<double>Mm;
 	int M=0;
 	double m=0.00;
 	cin>>M;
 		for(int i=0;i<M;i++)
	{
		cin>>m;
		Mm.push_back(m);	
	}
	if(A.size()>M)
	{
			for(int i=0;i<M;i++)
	{
		C.push_back(A[i]);
		C.push_back(Mm[i]);	
	}
	
			for(int i=M;i<A.size();i++)
	{
		C.push_back(A[i]);	
	}
	
	}
	else
	{
			for(int i=0;i<A.size();i++)
	{
		C.push_back(Mm[i]);
		C.push_back(A[i]);	
	}
	
			for(int i=A.size();i<M;i++)
	{
		C.push_back(Mm[i]);	
	}
	
	}
 	
 	A=C;	
 }
  void value_at(vector<double>&A)
 {
 	vector<double>Aa;
 	cin>>b>>c>>d;
	 for(int i=0;i<A.size();i++)
 	{
 		
	 
	 	if(b-1==i)
	 	Aa.push_back(A[i]);
	 	if(d-1==i)
	 	Aa.push_back(A[i]);
	 	if(c-1==i)
	 	Aa.push_back(A[i]);
	 
 		
	 }
	 A=Aa;
 }
 
 void print(vector<double>&A)
 {
 	for(int i=0;i<A.size();i++)
 	{
 		
 		if(i==A.size()-1)
 		cout<<A[i]<<" "<<endl;
 		else
 		cout<<A[i]<<" ";
	 }
 }


 void command(const string &cmd,vector<double>&A, const int &count)
 {
 	if(cmd=="add")
 	 exe_add(A);
 	else if(cmd=="remove")
 	    remove(A);
 	else if(cmd=="reverse")
 	    reverse(A,count);
 	else if(cmd=="shift_left")
	    shift_left(A); 
	else if(cmd=="shift_right")
		 shift_right(A); 
	else if(cmd=="merge")
	     merge(A);
    else if(cmd=="value_at")
	     value_at(A);
 	else if(cmd=="print")
		 print(A);
   
    
 }


int main()

{
	//code here
	vector<double>A(0);
	int n=0;
	double num=0.00;
	string cmd;
	cin>>n;
	for(int i=0;i<n;i++)
	{
		cin>>num;
		A.push_back(num);
		
	}
	int count=A.size();
	while(cin>>cmd)
	{
		command(cmd,A,count);
		if(cmd=="exit")
		break;
		
	}
	

	return 0;
}
